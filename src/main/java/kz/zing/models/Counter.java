package kz.zing.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

@Document(collection = "sber_counter")
public class Counter {
    @Id
    private ObjectId id;
    private Long count;

    public Counter inc() {
        count++;
        return this;
    }
}
