package kz.zing.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlRootElement;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

@XmlRootElement(name = "response")
public class Response {
    private Integer result;
    private String comment;

    private String info;
    private String fio;

    private Long ext_id;
    private String reg_date;
    private String sum;
}
