package kz.zing.repositories;

import kz.zing.models.User;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UsersRepository extends MongoRepository<User, ObjectId> {
    Optional<User> findByUid(String uid);
}
