package kz.zing.controllers;

import kz.zing.models.Response;
import kz.zing.models.SberResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

@RestController
public class Test {

    @GetMapping("/")
    public void test() throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(Response.class);
        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(Response.builder()
                .result(0)
                .comment(SberResult.resultCodes.get(0))
                .fio("Жижин Артур Петрович")
                .info("Оплата интернет")
                .sum("100.00")
                .build(), System.out);
    }
}

