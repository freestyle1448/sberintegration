package kz.zing.controllers;

import kz.zing.NotFoundException;
import kz.zing.models.CommandType;
import kz.zing.models.Request;
import kz.zing.models.Response;
import kz.zing.models.SberResult;
import kz.zing.services.MainService;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RestController;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.Callable;

@RestController
public class MainController {
    private final MainService service;
    private final Marshaller marshaller;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Date.class,
                new CustomDateEditor(new SimpleDateFormat("yyyyMMddHHmmss"), true, 14));
    }

    public MainController(MainService service) throws JAXBException {
        this.service = service;
        JAXBContext jaxbContext = JAXBContext.newInstance(Response.class);
        marshaller = jaxbContext.createMarshaller();
    }

    @ExceptionHandler({NotFoundException.class})
    public String handleException(NotFoundException ex) throws JAXBException {
        StringWriter sw = new StringWriter();
        marshaller.marshal(Response.builder()
                .result(ex.getCode())
                .comment(ex.getMessage())
                .build(), sw);
        return sw.toString();
    }

    @GetMapping("/zing_payment")
    public Callable<String> check(Request request) {
        return () -> {
            if (request.getCommand().equals(CommandType.CHECK)) {
                StringWriter sw = new StringWriter();
                marshaller.marshal(service.check(request).get(), sw);
                return sw.toString();
            } else if (request.getCommand().equals(CommandType.PAY)) {
                StringWriter sw = new StringWriter();
                marshaller.marshal(service.pay(request).get(), sw);
                return sw.toString();
            }

            StringWriter sw = new StringWriter();
            marshaller.marshal(Response.builder()
                    .result(2)
                    .comment(SberResult.resultCodes.get(2))
                    .build(), sw);
            return sw.toString();
        };
    }
}
