package kz.zing.models.transaction;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class Balance {
    private Number amount;
    private String currency;

}
