package kz.zing.services;

import kz.zing.NotFoundException;
import kz.zing.models.*;
import kz.zing.models.transaction.*;
import kz.zing.repositories.*;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

import static kz.zing.repositories.ManualRepository.SBER_GATE;

@Service
public class MainServiceImpl implements MainService {
    private final UsersRepository usersRepository;
    private final ManualRepository manualRepository;
    private final CounterRepository counterRepository;
    private final TransactionsRepository transactionsRepository;

    public MainServiceImpl(UsersRepository usersRepository, AccountsRepository accountsRepository, ManualRepository manualRepository, CounterRepository counterRepository, TransactionsRepository transactionsRepository) {
        this.usersRepository = usersRepository;
        this.manualRepository = manualRepository;
        this.counterRepository = counterRepository;
        this.transactionsRepository = transactionsRepository;
    }

    @Override
    public CompletableFuture<Response> check(Request request) {
        final Optional<User> userOptional = usersRepository.findByUid(request.getAccount());
        if (userOptional.isEmpty())
            return CompletableFuture.completedFuture(Response.builder()
                    .result(3)
                    .comment(SberResult.resultCodes.get(3))
                    .build());

        User user = userOptional.get();
        return CompletableFuture.completedFuture(Response.builder()
                .result(0)
                .comment(SberResult.resultCodes.get(0))
                .fio(String.format("%s %s %s", user.getCredentials().getSecondName(),
                        user.getCredentials().getFirstName(),
                        user.getCredentials().getMiddleName()))
                .info("Чаевые официанту")
                .build());
    }

    @Override
    public CompletableFuture<Response> pay(Request request) throws NotFoundException {
        Optional<Counter> counterOptional = counterRepository.findById(new ObjectId("5d52f5a28e2c7a1502118185"));
        Counter counter = null;

        if (counterOptional.isPresent())
            counter = counterOptional.get();

        //noinspection ConstantConditions
        Transaction transaction = Transaction.builder()
                .id(new ObjectId())
                .gateId(SBER_GATE)
                .transactionNumber(counter.inc().getCount())
                .amount(Balance.builder()
                        .amount((Double.valueOf(request.getSum() * 100)).longValue())
                        .currency("RUB")
                        .build())
                .commission(TransactionCommission.builder()
                        .bankAmount(0L)
                        .systemAmount(0L)
                        .currency("RUB")
                        .build())
                .finalAmount(Balance.builder()
                        .amount((Double.valueOf(request.getSum() * 100)).longValue())
                        .currency("RUB")
                        .build())
                .startDate(request.getTxn_date())
                .endDate(new Date())
                .type(Type.DONATE)
                .stage(1)
                .status(Status.SUCCESS)
                .build();

        counterRepository.save(counter);

        final Optional<User> userOptional = usersRepository.findByUid(request.getAccount());
        if (userOptional.isEmpty())
            return CompletableFuture.completedFuture(Response.builder()
                    .result(3)
                    .comment(SberResult.resultCodes.get(3))
                    .build());

        User user = userOptional.get();

        if (manualRepository.findAndModifyAccountAdd(user.getId(), transaction.getFinalAmount(), transaction.getId()) != null &&
                manualRepository.findAndModifyGateAdd(transaction.getGateId(), transaction.getFinalAmount()) != null) {
            transactionsRepository.save(transaction);

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
            return CompletableFuture.completedFuture(Response.builder()
                    .result(0)
                    .comment(SberResult.resultCodes.get(0))
                    .ext_id(transaction.getTransactionNumber())
                    .reg_date(user.getCreationDate() != null ?
                            dateFormat.format(user.getCreationDate()) : dateFormat.format(new Date()))
                    .sum(request.getSum().toString())
                    .build());
        } else
            throw new NotFoundException(300, "Ошибка при пополнении баланса Плательщика! " +
                    "Не был найден ID в системе или валюта баланса отлична от RUB");

    }
}
