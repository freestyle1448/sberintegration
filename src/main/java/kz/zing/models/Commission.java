package kz.zing.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
class Commission {
    private BigDecimal min;
    private BigDecimal percent;
    private BigDecimal max;
    private BigDecimal fee;
}
