package kz.zing.repositories;

import kz.zing.models.Account;
import kz.zing.models.Gate;
import kz.zing.models.transaction.Balance;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

@Repository
public class ManualRepository {
    private final MongoTemplate mongoTemplate;
    public static final ObjectId SBER_GATE = new ObjectId("5d95d01bdb8d502d6c9536b2");

    public ManualRepository(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    public Account findAndModifyAccountAdd(ObjectId userId, Balance balance, ObjectId transactionId) {
        Query findAndModifyAccount = new Query(Criteria
                .where("userId").is(userId)
                .and("balance.currency").is(balance.getCurrency()));
        Update update = new Update();
        update.inc("holdBalance.amount", balance.getAmount().longValue());
        update.addToSet("holdBalance.transactions", transactionId);

        return mongoTemplate.findAndModify(findAndModifyAccount, update, Account.class);
    }

    public Gate findAndModifyGateAdd(ObjectId gateId, Balance balance) {
        Query findAndModifyGate = new Query(Criteria.where("_id").is(gateId)
                .and("balance.currency").is(balance.getCurrency()));
        Update update = new Update();
        update.inc("balance.amount", balance.getAmount().longValue());

        return mongoTemplate.findAndModify(findAndModifyGate, update, Gate.class);
    }
}
