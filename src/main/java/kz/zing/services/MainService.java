package kz.zing.services;

import com.mongodb.MongoCommandException;
import kz.zing.NotFoundException;
import kz.zing.models.Request;
import kz.zing.models.Response;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.transaction.annotation.Transactional;

import java.util.concurrent.CompletableFuture;

@Async
public interface MainService {
    CompletableFuture<Response> check(Request request);

    @Retryable(
            value = {MongoCommandException.class},
            maxAttempts = 100, backoff = @Backoff(50))
    @Transactional(rollbackFor = NotFoundException.class)
    CompletableFuture<Response> pay(Request request) throws NotFoundException;
}
