package kz.zing.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Request {
    private String command;
    private String account;
    private Double sum;
    private Long txn_id;
    private Date txn_date;
}
