package kz.zing.models.transaction;

public final class Type {
    public static final String DONATE = "Donate";
    public static final String WITHDRAWAL = "Withdrawal";
    public static final String PARTNER_COMMISSION = "partnerCommission";
}
