package kz.zing;

public class NotFoundException extends Exception {
    private int code;
    private String msg;

    public NotFoundException(int code, String msg) {
        super(msg);
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }
}
