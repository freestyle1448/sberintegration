package kz.zing.models;

public final class CommandType {
    public static final String CHECK = "check";
    public static final String PAY = "pay";
}
