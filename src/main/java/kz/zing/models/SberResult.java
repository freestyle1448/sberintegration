package kz.zing.models;

import java.util.HashMap;
import java.util.Map;

public final class SberResult {
    public static final Map<Integer, String> resultCodes = new HashMap<>(14);

    static {
        resultCodes.put(0, "Успешное завершение операции");
        resultCodes.put(1, "Временная ошибка. Повторите запрос позже");
        resultCodes.put(2, "Неизвестный тип запроса");
        resultCodes.put(3, "Плательщик не найден");
        resultCodes.put(4, "Неверный формат идентификатора Плательщика");
        resultCodes.put(5, "Счет Плательщика не активен");
        resultCodes.put(6, "Неверное значение идентификатора транзакции");
        resultCodes.put(7, "Прием платежа запрещен по техническим причинам");
        resultCodes.put(8, "Дублирование транзакции");
        resultCodes.put(9, "Неверная сумма платежа");
        resultCodes.put(10, "Сумма слишком мала");
        resultCodes.put(11, "Сумма слишком велика");
        resultCodes.put(12, "Неверное значение даты");
        resultCodes.put(300, "Внутренняя ошибка Организации");
    }
}
